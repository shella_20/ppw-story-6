from django.db import models
from django.utils import timezone
import datetime

# Create your models here.
class Status(models.Model):
	timeField = models.DateTimeField(default=timezone.now)
	textField = models.CharField(max_length=300)

	def __str__(self):
		return self.textField