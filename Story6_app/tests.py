from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import textViews
from . import models
from . import forms
from selenium import webdriver
from . import views

from selenium.webdriver.chrome.options import Options

    
class UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options)
        self.browser = webdriver.Chrome(chrome_options=options)
        

    def tearDown(self):
        self.browser.close()

    def test_halo_text(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        #self.browser.get('http://localhost:8000')
        self.browser.get('http://shella-ppw-story6.herokuapp.com')

        halo = self.browser.find_element_by_class_name('test_halo').text
        self.assertEquals(halo, "Halo apa kabar!")
        # self.browser.close()

    def test_title(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        #self.browser.get('http://localhost:8000')
        self.browser.get('http://shella-ppw-story6.herokuapp.com')

        self.assertEquals(self.browser.title, 'Homepage Status')
        # self.browser.close()

    def test_bisa_bikin_status(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        #self.browser.get('http://localhost:8000')
        self.browser.get('http://shella-ppw-story6.herokuapp.com')

        status_form = self.browser.find_element_by_id("textField")
        submit = self.browser.find_element_by_id("submit_button")
        status_form.send_keys("test_text")
        submit.click()
        self.assertIn( "test_text", self.browser.page_source)
        # self.browser.close()

    def test_tambah_status_pada_halaman(self):
        statusMessage = 'test_text'
        response = self.client.post('/', {
            'textField': statusMessage
        })
        content = self.client.get('/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(models.Status.objects.all().count(), 1)
        self.assertIn(
            statusMessage, content
        )

    def test_models_punya_str(self):
        status = models.Status.objects.create(
            textField= 'test_text'
        )
        self.assertEqual(str(status), 'test_text')

        #test url '/' exist or not
    def test_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

        #test url using status view function
    def test_calling_right_views_function(self):
        found_views = resolve('/')
        self.assertEqual(found_views.func, views.textViews)

        #test '/' using templates index.html
    def test_using_right_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'homepage.html')
