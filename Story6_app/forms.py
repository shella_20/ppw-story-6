from django import forms
from . import models
import datetime

class TextForms(forms.ModelForm):
	class Meta:
		model =  models.Status
		fields =  ["textField"]
	def __init__(self, *args, **kwargs):
		super(TextForms, self).__init__(*args, **kwargs)

		self.fields["textField"].widget.attrs['id'] = "textField"