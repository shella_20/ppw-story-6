from django.shortcuts import render, redirect
from . import forms
from .models import Status
from django.utils import timezone
import datetime

# Create your views here.
def textViews(request):
	if request.method == "POST":
		form = forms.TextForms(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Story6_app:textViews')
	form = forms.TextForms()
	status =  Status.objects.all()
	return render(request, "homepage.html", {'form':form, 'status':status})