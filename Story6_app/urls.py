from django.urls import path
from django.contrib import admin
from . import views

app_name = "Story6_app"

urlpatterns = [
	path ('', views.textViews, name='textViews')
]